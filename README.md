# filmography

A command line tool to manage your filmography

## Roadmap

- [x] Add a movie
- [x] Deleted a movie
- [x] List the movies
  - [x] Sorted movies
  - [ ] Filtered movies
- [ ] Edit a movie
- [ ] Export movie list

## License

[MIT](https://choosealicense.com/licenses/mit/)