pub mod movie {
	use std::cmp::Ordering;

	use serde::{Deserialize, Serialize};
	use structopt::clap::arg_enum;

	#[derive(Deserialize, Serialize, Debug)]
	pub struct Movie {
		title: String,
		rating: Option<f32>,
		date: Option<chrono::NaiveDate>,
		genre: Option<Genre>,
		country: Option<Country>,
		duration: Option<u16>,
	}

	impl Movie {
		pub fn new(title: String, rating: Option<f32>, date: Option<chrono::NaiveDate>, genre: Option<Genre>, country: Option<Country>, duration: Option<u16>) -> Movie {
			Movie { title, rating, date, genre, country, duration }
		}
		pub fn get_title(&self) -> &String {
			&self.title
		}
		pub fn cmp_title(&self, other: &Movie) -> Ordering {
			self.title.cmp(&other.title)
		}
		pub fn cmp_rating(&self, other: &Movie) -> Ordering {
			self.rating.partial_cmp(&other.rating).unwrap_or(Ordering::Equal)
		}
		pub fn cmp_date(&self, other: &Movie) -> Ordering {
			self.date.cmp(&other.date).reverse()
		}
		pub fn cmp_genre(&self, other: &Movie) -> Ordering {
			self.genre.cmp(&other.genre)
		}
		pub fn cmp_country(&self, other: &Movie) -> Ordering {
			self.country.cmp(&other.country)
		}
		pub fn cmp_duration(&self, other: &Movie) -> Ordering {
			self.duration.cmp(&other.duration).reverse()
		}
	}

	impl std::fmt::Display for Movie {
		fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
			write!(f, "title: \"{}\"", self.title)?;
			if let Some(d) = &self.date { write!(f, ", date: {}", d)? }
			if let Some(g) = &self.genre { write!(f, ", genre: {}", g)? }
			if let Some(r) = &self.rating { write!(f, ", rating: {}", r)? }
			if let Some(c) = &self.country { write!(f, ", country: {}", c)? }
			if let Some(d) = &self.duration { write!(f, ", duration: {}", d)? }
			write!(f, "")
		}
	}

	arg_enum! {
		#[derive(Serialize, Deserialize, Debug, Ord, PartialOrd, Eq, PartialEq)]
		pub enum Genre { Action, Adventure, Animation, Biography, Comedy, Crime, Drama, Family, Fantasy, History, Horror, Mystery, Romance, SciFi, Sport, Thriller, War, Western }
	}

	arg_enum! {
		#[derive(Serialize, Deserialize, Debug, Ord, PartialOrd, Eq, PartialEq)]
		pub enum Country { US, UK, SF, SZ, KS, AG, BE, GE, ES, FR, JP }
	}

	arg_enum! {
		#[derive(Serialize, Deserialize, Debug)]
		pub enum SortingMethod { Title, Rating, Date, Genre, Country, Duration }
	}
}

pub mod data {
	use std::io::{Read, Write};

	use super::movie::Movie;

	pub fn load() -> Result<Vec<Movie>, Box<dyn std::error::Error>> {
		match std::fs::File::open("filmography.csv") {
			Ok(mut file) => {
				let mut contents = String::new();
				file.read_to_string(&mut contents)?;
				file.flush()?;

				let mut reader = csv::Reader::from_reader(contents.as_bytes());
				let mut result = vec![];

				for movie in reader.deserialize() {
					let movie: Movie = movie?;
					result.push(movie);
				}
				Ok(result)
			}
			Err(_) => {
				let mut file = std::fs::File::create("filmography.csv")?;
				file.write(b"title,rating,date,genre,country,duration")?;
				file.flush()?;
				Ok(vec![])
			}
		}
	}

	pub fn save(filmography: &Vec<Movie>) -> Result<(), Box<dyn std::error::Error>> {
		let mut wtr = csv::Writer::from_path("filmography.csv")?;
		filmography.iter().try_for_each(|movie| wtr.serialize(&movie))?;
		wtr.flush()?;
		Ok(())
	}
}

pub mod cli {
	use structopt::{clap::AppSettings, StructOpt};

	use super::movie::{Country, Genre, SortingMethod};

	#[derive(StructOpt, Debug)]
	#[structopt(name = "filmography", setting = AppSettings::InferSubcommands)]
	pub enum Opt {
		Add {
			title: String,
			#[structopt(long)]
			rating: Option<f32>,
			#[structopt(long, help = "The release date of the movie (YYYY-MM-DD)")]
			date: Option<chrono::NaiveDate>,
			#[structopt(long, possible_values = & Genre::variants(), case_insensitive = true)]
			genre: Option<Genre>,
			#[structopt(long, possible_values = & Country::variants(), case_insensitive = true)]
			country: Option<Country>,
			#[structopt(long)]
			duration: Option<u16>,
		},
		Remove {
			title: String,
			#[structopt(short, long)]
			yes: bool,
		},
		List {
			#[structopt(possible_values = & SortingMethod::variants(), case_insensitive = true, name = "SORT METHOD", default_value = "title")]
			sort_method: SortingMethod,
		},
	}
}