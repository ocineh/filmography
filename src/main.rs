use std::io::Write;

use structopt::StructOpt;

use filmography::{
	cli::Opt,
	data::{load, save},
	movie::{Movie, SortingMethod},
};

fn run() -> Result<(), Box<dyn std::error::Error>> {
	let opt: Opt = Opt::from_args();
	println!("opt: {:?}", opt);
	match opt {
		Opt::Add { title, rating, date, genre, country, duration } => {
			let mut movies = load()?;
			movies.push(Movie::new(title, rating, date, genre, country, duration));
			save(&movies)?;
			Ok(())
		}
		Opt::Remove { title, mut yes } => {
			let mut movies = load()?;
			let title = title.trim().to_lowercase();
			let index = movies.iter().position(|movie| movie.get_title() == &title);

			match index {
				None => println!("Not found"),
				Some(index) => {
					loop {
						match yes {
							true => {
								println!("The movie \"{}\" has been suppressed.", movies[index].get_title());
								movies.remove(index);
								save(&movies)?;
								break;
							}
							false => {
								print!("Do you want to delete the movie \"{}\" (Y)es/(N)o: ", movies[index].get_title());
								std::io::stdout().flush().unwrap();

								let mut response = String::new();
								std::io::stdin().read_line(&mut response)?;

								match response.trim().to_lowercase().as_str() {
									n if ["yes", "y"].contains(&n) => yes = true,
									n if ["no", "n"].contains(&n) => break,
									_ => {}
								}
							}
						}
					}
				}
			}
			Ok(())
		}
		Opt::List { sort_method } => {
			let mut movies = load()?;
			match sort_method {
				SortingMethod::Title => {
					println!("Movie sorted by title");
					movies.sort_by(|a, b| a.cmp_title(&b))
				}
				SortingMethod::Rating => {
					println!("Movie sorted by rating");
					movies.sort_by(|a, b| a.cmp_rating(&b))
				}
				SortingMethod::Date => {
					println!("Movie sorted by date");
					movies.sort_by(|a, b| a.cmp_date(&b))
				}
				SortingMethod::Genre => {
					println!("Movie sorted by genre");
					movies.sort_by(|a, b| a.cmp_genre(&b))
				}
				SortingMethod::Country => {
					println!("Movie sorted by country");
					movies.sort_by(|a, b| a.cmp_country(&b))
				}
				SortingMethod::Duration => {
					println!("Movie sorted by duration");
					movies.sort_by(|a, b| a.cmp_duration(&b))
				}
			}
			movies.iter().enumerate().for_each(|(i, m)| println!("({}): {}", i, m));
			Ok(())
		}
	}
}

fn main() {
	if let Err(err) = run() { eprintln!("Error: {}", err) }
}